import numpy
import math

class attitude_simulator:
    """
       attitude simulator class. \\
       @param quaternion: initial unit quaternion. \\
       @param angular_rate: initial angular rate, rad/s. \\
       @param step_size: simulation step size, seconds. \\
       @param moi: moment of inertia of the satellite.
    """

    def __init__( self, quaternion, anuglar_rate, step_size, moi ):
        """
            constructor
        """
        # get initial state
        self.q = quaternion
        self.w = anuglar_rate

        # set step size, seconds
        self.dt = step_size

        # get moment of inertia
        self.J = moi

    def transform(self, q):
        """
            body to ECI transformation. \\
            @param q: quaternion \\
            @return A: rotation matrix from body to ECI \\
            Ref: CubeSat Attitude Determination via Kalman Filtering of Magnetometer and Solar Cell Data
        """
        A = numpy.zeros((3,3))
        A[0,0] = q[0]**2 + q[1]**2 - q[2]**2 - q[3]**2
        A[0,1] = 2 * (q[1]*q[2] - q[0]*q[3])
        A[0,2] = 2 * (q[1]*q[3] + q[0]*q[2])

        A[1,0] = 2 * (q[1]*q[2] + q[0]*q[3])
        A[1,1] = q[0]**2 - q[1]**2 + q[2]**2 - q[3]**2
        A[1,2] = 2 * (q[2]*q[3] - q[0]*q[1])

        A[2,0] = 2 * (q[1]*q[3] - q[0]*q[2])
        A[2,1] = 2 * (q[2]*q[3] + q[0]*q[1])
        A[2,2] = q[0]**2 - q[1]**2 - q[2]**2 + q[3]**2

        return A

    def Dynamics(self, omega, T):
        """
            Euler rotation equation. \\
            T = torque. \\
            omega = angular velcotity. \\
            self.J = moment of inertia. \\
            Ref: Markley, Landis & Crassidis, John. (2014). Fundamentals of Spacecraft Attitude Determination and Control
        """
        wdot = numpy.linalg.inv(self.J) @ ( T - numpy.cross( omega, self.J @ omega)  ) 
        return wdot 

    def quatKin(self, q, wquat):
        """
            Quaternion kinematics. \\
            q[0] is the scalar component. \\
            Ref: CubeSat Attitude Determination via Kalman Filtering of Magnetometer and Solar Cell Data
        """
        E = numpy.zeros((4,3))
        E[0,0] = -q[1]
        E[0,1] = -q[2]
        E[0,2] = -q[3]
        E[1,0] = q[0]
        E[1,1] = -q[3]
        E[1,2] = q[2]
        E[2,0] = q[3]
        E[2,1] = q[0]
        E[2,2] = -q[1]
        E[3,0] = -q[2]
        E[3,1] = q[1]
        E[3,2] = q[0]
        qdot = 0.5 * E @ wquat
        return qdot

    def att_RK4(self, torque_input):
        """
            4th fixed step runge-kutta attitude simulator \\
            @param torque_input: current torque \\
            @return q: quaternion \\
            @return w: angular velocity, rad/s \\
            Ref: http://ai.stanford.edu/~varung/rigidbody.pdf
        """
        #!!!!!!!!!!!!!!!!!!!!!!!!! TO DO: complete the RK4 for euler dynamics !!!!!!!!!!!!!!!!!!!!!!!!!#
        k1 = self.dt * self.Dynamics( self.w, torque_input )
        k2 = self.dt * self.Dynamics( self.w + k1*0.5, torque_input )
        k3 = self.dt * self.Dynamics( self.w + k2*0.5, torque_input )
        k4 = self.dt * self.Dynamics( self.w + k3, torque_input )
        

        #!!!!!!!!!!!!!!!!!!!!!!!!! TO DO: complete the RK4 for quaternion kinematics !!!!!!!!!!!!!!!!!!!!!!!!!#
        k1q = self.dt * self.quatKin( self.q, self.w )
        k2q = self.dt * self.quatKin( self.q + k1q*0.5, self.w + k1*0.5 )
        k3q = self.dt * self.quatKin( self.q + k2q*0.5, self.w + k2*0.5 )
        k4q = self.dt * self.quatKin( self.q + k3q, self.w + k3 )

        self.w = self.w + (1/6) * ( k1 + 2*k2 + 2*k3 + k4 )
        self.q = self.q + (1/6) * ( k1q + 2*k2q + 2*k3q + k4q )
        #renormalize quaternions
        q_mag = math.sqrt( self.q[0]**2 + self.q[1]**2 + self.q[2]**2 + self.q[3]**2 )
        self.q = self.q / q_mag

        #!!!!!!!!!!!!!!!!!!!!!!!!! TO DO: complete the return values !!!!!!!!!!!!!!!!!!!!!!!!!#
        return self.q, self.w

    def quaternion_to_euler(self, sequence):
        """
            convert hamilton quaternion to 3-1-3 euler or 1-2-3 euler
            @source: Euler Angles, Quaternions, and Transformation Matrices
        """  
        q1 = self.q[0]
        q2 = self.q[1]
        q3 = self.q[2]
        q4 = self.q[3]

        # rotation matrix
        m11 = (q1**2 + q2**2 - q3**2 -q4**2)
        m12 = 2 * (q2*q3 - q1*q4)
        m13 = 2 * (q2*q4 + q1*q3)
        m23 = 2 * (q3*q4 - q1*q2)
        m31 = 2 * (q2*q4 -q1*q3)
        m32 = 2 * (q3*q4 + q1*q2)
        m33 = (q1**2 - q2**2 - q3**2 + q4**2)
        
        # get euler from quat
        euler = numpy.zeros(3)
        if sequence == 321:
            euler[2] = -math.atan2( -m23, m33 )
            euler[1] = -math.atan2( m13, math.sqrt(1-m13**2) )
            euler[0] = -math.atan2( -m12, m11 )
        else: 
            euler[2] = -math.atan2( m13, -m23 )
            euler[1] = -math.atan2( math.sqrt(1-m33**2), m33 )
            euler[0] = -math.atan2( m31, m32 )
        
        return euler