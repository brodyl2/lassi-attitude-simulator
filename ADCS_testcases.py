import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.animation import FuncAnimation
from ADCSSimulator import attitude_simulator

fig, ax = plt.subplots(subplot_kw=dict(projection="3d"))

def get_arrow(theta):
    x = 0
    y = 0
    z = 0

    q = np.array([1,0,0,0])
    w = np.array([-10,0,0])
    moi = np.array([[1,0,0], [0,1,0], [0,0,1]])
    Torque = np.array([5,0,0])
    pos0 = np.array([0,1,0])
    i=0
    while i<theta:
        q, w = attitude_simulator(q, w, 0.05, moi).att_RK4(Torque)
        i+=1
    pos_t = attitude_simulator(q, w, 0.05, moi).transform(q) @ pos0

    u = pos_t[0]
    v = pos_t[1]
    j = pos_t[2]
    return x,y,z,u,v,j

quiver = ax.quiver(*get_arrow(0))

ax.set_xlim(-1, 1)
ax.set_ylim(-1, 1)
ax.set_zlim(-1, 1)

def update(theta):
    global quiver
    quiver.remove()
    quiver = ax.quiver(*get_arrow(theta))

ani = FuncAnimation(fig, update, frames=np.linspace(0,120,121), interval=1)
plt.show()